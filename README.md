## [Download Complete Zip](https://gitlab.com/artofsarahb/the-lincoln-experience/repository/archive.zip?ref=master)

* ### [Project Overview](https://gitlab.com/artofsarahb/the-lincoln-experience/raw/master/Oculus_Submit/sarahBarrick_TLE_ProjectOverviewBrief.pdf)

* ### [Process Images](https://gitlab.com/artofsarahb/the-lincoln-experience/raw/master/Oculus_Submit/sarahBarrick_TLE_ProcessImages.pdf)

* ### [Running Build Video](https://gitlab.com/artofsarahb/the-lincoln-experience/raw/master/Oculus_Submit/sarahBarrick_TLE_runningBuild.mp4)

* ### [Unity Scene Current Render](https://gitlab.com/artofsarahb/the-lincoln-experience/raw/master/Assets/Scenes/TLE_Build.unity)

* ### [Greybox GearVR APK](https://gitlab.com/artofsarahb/the-lincoln-experience/raw/master/Oculus_Submit/SarahBarrick_TLE_greybox.apk)
##### Greybox is a proof concept VR build. Current scene is crashing on GearVr (logcat [here](https://gitlab.com/artofsarahb/the-lincoln-experience/blob/master/unity_crash_log.txt.txt))