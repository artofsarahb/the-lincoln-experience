﻿using UnityEngine;
using System.Collections;

public class SpeecTrigger_Speech_Trigger : MonoBehaviour {

    private bool triggered;

    void Start()
    {
        triggered = false;
    }

    // Use this for initialization
    void OnTriggerEnter(Collider other)
    {
        AudioSource soundFile = GameObject.Find("Lincoln_Proxy").GetComponent<AudioSource>();
        Light spotlight = GameObject.Find("Spotlight").GetComponent<Light>();
        Debug.Log(spotlight.type);
        //Debug.Log(other.gameObject.name);
        if (other.gameObject.name == "Player" && !triggered)
        {
            spotlight.enabled = false;
            soundFile.Play();
            triggered = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        Light spotlight = GameObject.Find("Spotlight").GetComponent<Light>();
        if (other.gameObject.name == "Player")
        {
            spotlight.enabled = true;
        }
    }
}
