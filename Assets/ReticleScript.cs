﻿using UnityEngine;
using System.Collections;

public class ReticleScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        var reticle = GameObject.Find("Reticle Component").GetComponent<Transform>();

        var pos = reticle.position;

        pos.y = 1.5f;

        reticle.position = pos;

	}
}
