﻿using UnityEngine;
using System.Collections;

public class SpeechTrigger_Speech_Trigger : MonoBehaviour {

	// Use this for initialization
    void OnTriggerEnter(Collider other)
    {
        Debug.Log("k");
        AudioSource soundFile = GameObject.Find("Lincoln_Proxy").GetComponent<AudioSource>();
        Light spotlight = GameObject.Find("Spotlight").GetComponent<Light>();
        //Debug.Log(spotlight.type);
        //Debug.Log(other.gameObject.name);
        if (other.gameObject.name == "Player")
        {
            Debug.Log("b");
            spotlight.enabled = false;
            soundFile.Play();
        }
    }
}
